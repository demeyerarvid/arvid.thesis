(ns arvid.thesis.plugin.clj.strategies.strategyFactory
  (:require [arvid.thesis.plugin.clj.strategies.strategy :as strategy])
  (:require [arvid.thesis.plugin.clj.strategies.helpers.grouping :as grouping])
  (:require [arvid.thesis.plugin.clj.strategies.helpers.templates :as templates])
  (:require [arvid.thesis.plugin.clj.strategies.helpers.equalities :as equalities])
  (:require [arvid.thesis.plugin.clj.changenodes.change :as change])
  (:require [damp.ekeko.snippets.matching :as matching])
  (:require [damp.ekeko.snippets.snippet :as snippet])
  (:require [damp.ekeko.snippets.snippetgroup :as snippetgroup])
  (:require [arvid.thesis.plugin.clj.preprocess.generalization.gengroup :as gengroup])
  (:require [arvid.thesis.plugin.clj.preprocess.grouping.group :as group]))

; Equalities for operations
;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn
  equals-operation-fully?
  "Returns whether the same type of operation is performaed."
  [group1 change1 group2 change2]
  (= (:operation change1) (:operation change2)))

(defn
  equals-operation-who-cares?
  "Returns true."
  [group1 change1 group2 change2]
  true)

; Equalities for change subjects
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn
  equals-subject-fully? 
  "Returns whether the subtree rooted at the given node fully matches the given other node." 
  [group1 change1 group2 change2]
  (let [node1 (change/get-subject change1)
			  node2 (change/get-subject change2)]
    (equalities/equals-subtree? Integer/MAX_VALUE true false node1 node2)))

(defn
  equals-subject-structurally? 
  "Returns whether the subtree rooted at the given node structurally matches the given other node: does not take
   propertyvalues of SimpleProperty's into consideration." 
  [group1 change1 group2 change2]
  (let [node1 (change/get-subject change1)
			  node2 (change/get-subject change2)]
    (equalities/equals-subtree? Integer/MAX_VALUE false false node1 node2)))

(defn
  equals-subject-structurally-only-mandatory? 
  "Returns whether the subtree rooted at the given node structurally matches the given other node: does not take
   propertyvalues of SimpleProperty's into consideration." 
  [group1 change1 group2 change2]
  (let [node1 (change/get-subject change1)
			  node2 (change/get-subject change2)]
    (equalities/equals-subtree? Integer/MAX_VALUE false true node1 node2)))

(defn
  equals-subject-depth-limited-structurally-2? 
  "Returns whether the subtree rooted at the given node partially structurally matches the given other node: does not take
   propertyvalues of SimpleProperty's into consideration. Only looks 2 levels deep." 
  [group1 change1 group2 change2]
  (let [node1 (change/get-subject change1)
			  node2 (change/get-subject change2)]
    (equalities/equals-subtree? 2 false false node1 node2)))

(defn
  equals-subject-type? 
  "Returns whether the node's type matches the given other node's type."
  [group1 change1 group2 change2]
  (let [node1 (change/get-subject change1)
			  node2 (change/get-subject change2)]
    (equalities/equals-subtree? 1 false false node1 node2)))

(defn
  equals-subject-who-cares? 
  "Considers all nodes equal."
  [group1 change1 group2 change2]
  (let [node1 (change/get-subject change1)
			  node2 (change/get-subject change2)]
    (equalities/equals-subtree? 0 false false node1 node2)))

; Context-introducing equalities
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn
  equals-context-fully-changes-ignore?
  "Considers two nodes equal if their context within the group is fully equal."
  [group1 change1 group2 change2]
  (equalities/context-equals-subtree? true true group1 (gengroup/get-container group1) group2 (gengroup/get-container group2)))

(defn
  equals-context-structurally-changes-ignore?
  "Considers two nodes equal if their context within the group is structurally equal."
  [group1 change1 group2 change2]
  (equalities/context-equals-subtree? false true group1 (gengroup/get-container group1) group2 (gengroup/get-container group2)))

(defn
  equals-context-fully-changes-context?
  "Considers two nodes equal if their context within the group is fully equal."
  [group1 change1 group2 change2]
  (equalities/context-equals-subtree? true false group1 (gengroup/get-container group1) group2 (gengroup/get-container group2)))

(defn
  equals-context-structurally-changes-context?
  "Considers two nodes equal if their context within the group is structurally equal."
  [group1 change1 group2 change2]
  (equalities/context-equals-subtree? false false group1 (gengroup/get-container group1) group2 (gengroup/get-container group2)))

(defn
  equals-context-path-exact?
  "Considers two nodes equal if their exact path within the group is equal."
  [group1 change1 group2 change2]
  (let [path1 (group/get-path-of-change group1 change1)
        path2 (group/get-path-of-change group2 change2)]
    (= path1 path2)))

; TODO
;(defn
;  equals-context-path-relative?
;  "Considers two nodes equal if their relative path within the group is equal.
;   Note: the relative path does not include indices in lists."
;  [group1 change1 group2 change2]
;  true)

(defn
  equals-context-who-cares? 
  "Considers two nodes equal no matter their context within the group."
  [group1 change1 group2 change2]
  true)

; Template generation
;;;;;;;;;;;;;;;;;;;;;

(defn
  updater-1
  [s update-snippet pattern]
  (letfn [(snippet-traveller
           [s current]
           (cond (snippet/snippet-value-null? s current)
                   nil
                 (snippet/snippet-value-list? s current)
                   (doall (map (fn [child] (if (snippet-traveller s child)
                                               (update-snippet templates/apply-operator-simple current "replace-by-wildcard")))
                               (snippet/snippet-value-list-unwrapped s current)))
                 (snippet/snippet-value-node? s current)
                   (doall (map (fn [child] (if (snippet-traveller s child)
                                                   (update-snippet templates/apply-operator-simple current "replace-by-wildcard")))
                               (snippet/snippet-node-children s current)))
                 (snippet/snippet-value-primitive? s current)
                  (do true)))]
    (snippet-traveller s (snippet/snippet-root s))))

; Lookup table for lhs generation
(defn 
  get-lhs-updater 
  [equalities]
  updater-1)

;;;;;;;;;;;;
; Public API
;;;;;;;;;;;;

; Valid equalities:
;   :equals-operation-fully?
;   :equals-operation-who-cares?
;   :equals-subject-fully? 
;   :equals-subject-structurally? 
;   :equals-subject-structurally-only-mandatory?
;   :equals-subject-depth-limited-structurally-2? 
;   :equals-subject-type? 
;   :equals-subject-who-cares? 
;   :equals-context-fully-changes-ignore?
;   :equals-context-structurally-changes-ignore?
;   :equals-context-fully-changes-context?
;   :equals-context-structurally-changes-context?
;   :equals-context-path-exact?
;   :equals-context-path-relative?
;   :equals-context-who-cares? 
; TODO: mandatory shit
(defn-
  get-equality 
  [equality-keyword] 
  (resolve (symbol "arvid.thesis.plugin.clj.strategies.strategyFactory" (name equality-keyword))))

(defn
  make-strategy 
  ([]
   (make-strategy :MethodDeclaration #{:equals-operation-fully? :equals-subject-fully? :equals-context-path-exact?}))
  ([equalities]
   (make-strategy :MethodDeclaration equalities))
  ([group-container-type equalities]
	  (let [equality-functions (map get-equality equalities)] 
      (letfn [; Generalization strategy
              (generalizer-equals?
							  [group1 change1 group2 change2]
                ; Perform generalization based on all passed equalities (operation, subject and context)
							  (every? true? 
                        (map (fn [f] (f group1 change1 group2 change2))
                             equality-functions)))
              ; Grouping strategy
              (get-group-container
                [change]
                ; Return an AST, nil (no group container) or :unknown (couldn't determine with the information I received)
                (if (change/get-original change)
                    (grouping/get-ancestor-of-type group-container-type (change/get-original change))
                    :unknown))
              ; LHS construction
              (make-lhs
                [pattern]
                (let [lhs-updater (get-lhs-updater equalities)]
                  (if lhs-updater
                      (templates/make-lhs lhs-updater pattern)
                      (println "! LHS CONSTRUCTION IS NOT IMPLEMENTED FOR THIS STRATEGY. IGNORING PATTERN."))))]
			  ; Export the strategy
				(strategy/make generalizer-equals? get-group-container make-lhs)))))
		
